import React from 'react'

export const CityImageComponent = ({ imageUrl, imageDescription }) =>
    <img className="city-image" alt={imageDescription} src={imageUrl} />

export const CityImage = CityImageComponent