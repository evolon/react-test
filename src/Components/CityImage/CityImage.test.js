import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import { CityImageComponent } from './CityImage'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('<CityImageComponent />', () => {
    it('should match the snapshot', async () => {
        const props = {
            imageUrl: 'test:url',
            imageDescription: 'Test Description'
        }

        expect(shallow(<CityImageComponent {...props} />)).toMatchSnapshot()
    })
})