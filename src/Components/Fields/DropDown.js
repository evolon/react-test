import React from 'react'
import { withState } from 'recompose';
import './style.css'
import { compose, lifecycle } from 'recompose'

const DropDownComponent = ({value, onChange, listStatus, changeListStatus, options, icon}) =>
    <div className="input-container input-dropdown" onClick={e => {
        e.stopPropagation()
        changeListStatus(listStatus === 'opened' ? 'closed' : 'opened')
    }}>
        <span className="content input-dropdown-selected">
            {options.find(option => option.value === value).text}
        </span>
        { 
            icon ?
                <span className="icon">
                    {icon}
                </span> :
                <svg className="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M7 10l5 5 5-5z" className="shape" />
                    <path d="M0 0h24v24H0z" fill="none"/>
                </svg> 
        }
        {
            options && options.length > 0 && listStatus === 'opened' ?
                <ul className="input-dropdown-list">
                    {options.map(option => 
                        <li key={option.value} onClick={e => {
                                e.stopPropagation()
                                changeListStatus('closed')
                                onChange(option)
                            }}>
                            {option.text}
                        </li>
                    )}
                </ul>
            : null
        }
    </div>

const enhance = compose(
    withState('listStatus', 'changeListStatus', 'closed'),
    lifecycle({
        componentDidMount() {
            window.addEventListener('click', e => {
                this.props.changeListStatus('closed')
            })
        },
    })
)

export const DropDown = enhance(DropDownComponent)



