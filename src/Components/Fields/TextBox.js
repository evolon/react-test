import React from 'react'
import './style.css'
import { withProps } from 'recompose';
import { Spinner } from '../Spinner/Spinner'

const TextBoxComponent = ({icon, value, onChange, onIconClick, isDisabled, placeholder}) =>
    <div className="input-container">
        <input type="text" 
            className="content"
            disabled={isDisabled}
            value={value} 
            onChange={onChange}
            placeholder={placeholder} />
        {icon ? 
            <div className="icon" onClick={onIconClick}>
                {icon} 
            </div>
            : null
        }
    </div>

const enhance = withProps(() => ({
    LoadingSpinner: Spinner
}))

export const TextBox = enhance(TextBoxComponent)