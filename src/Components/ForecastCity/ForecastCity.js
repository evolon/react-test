import React from 'react'
import { compose, withProps } from 'recompose'
import { Search } from '../Search/Search'
import { WeatherIcon } from '../WeatherIcon/WeatherIcon'
import { KelvinTemp } from '../KelvinTemp/KelvinTemp'
import { CityImage } from '../CityImage/CityImage'
import moment from 'moment'
import './style.css'

const ForecastCityComponent = ({ weather, isLoading, hasWeatherData, backgroundImage, loadCityForecast, error }) => 
    <section className="forecast-city">
        <CityImage imageUrl={backgroundImage} />
        <Search isLoading={isLoading} loadCityForecast={loadCityForecast} error={error} />
        {
            hasWeatherData ?
                <div className="city-info">
                    <p className="today">
                        { moment(weather.list[0].dt, 'X').format('dddd D MMMM') }
                    </p>
                    <h1 className="city-name">
                        { weather.city.name }
                    </h1>
                </div>
                : null
        }
        {
            hasWeatherData ?
                <h2 className="city-weather">
                    <WeatherIcon weather={weather.list[0].weather[0]} />
                    <span className="city-temp">
                        <KelvinTemp temp={weather.list[0].main.temp} />
                    </span>
                </h2>
            : null
        }
    </section>

const enhance = compose(
    withProps(({image, weather}) => ({
            backgroundImage: image && image.data
                ? image.data.urls.regular /*`url('${image.data.urls.regular}')`*/
                : '',
            hasWeatherData: weather && weather.list && weather.list.length
        })
    )
)

export const ForecastCity = enhance(ForecastCityComponent)