import React from 'react'
import moment from 'moment'
import { KelvinTemp } from '../KelvinTemp/KelvinTemp'
import { WeatherIcon } from '../WeatherIcon/WeatherIcon'
import './style.css'

const ForecastDayComponent = ({day}) =>
    <div className="forecast-day">
        <h3>
            {moment(day.dt, 'X').format('ddd')}
        </h3>
        <WeatherIcon weather={day.weather[0]} />
        <p className="max-temp">
            <KelvinTemp temp={day.main.temp_max} />
        </p>
        <p className="min-temp">
            <KelvinTemp temp={day.main.temp_min} />
        </p>
    </div>

export const ForecastDay = ForecastDayComponent