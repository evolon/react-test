import React from 'react'
import { ForecastDay } from '../ForecastDay/ForecastDay'
import { withState, compose, renderNothing, branch } from 'recompose'
import './style.css'

const ForecastDaysComponent = ({showLimit, changeLimit, weather}) => 
    <section className="forecast-days-container">
        <p>
            Select the amount of days you want to show
        </p>
        <input name="showLimit" type="range" min="1" max="5" value={showLimit} onChange={e => changeLimit(e.target.value)} />
        <ul className="forecast-days">
            { weather.list.slice(0, showLimit).map(day =>
                <li key={day.dt.toString()}>
                    <ForecastDay day={day} />
                </li>
            ) }
        </ul>
    </section>

const enhance = compose(
    withState('showLimit', 'changeLimit', 3),
    branch(({ weather }) => !weather || !weather.list || weather.list.length <= 0,
        renderNothing
    )
)

export const ForecastDays = enhance(ForecastDaysComponent)