import React from 'react'

export const KelvinTempComponent = ({temp}) =>
    <span className="kelvin-temp">
        <span className="temp-number">{Math.round(temp - 273.15)}</span>
        <span className="temp-mark"> °C</span>
    </span>

export const KelvinTemp = KelvinTempComponent
