import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import { KelvinTempComponent } from './KelvinTemp'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('<KelvinTempComponent />', () => {
    it('should match the snapshot', async () => {
        const props = {
            temp: 0
        }

        expect(shallow(<KelvinTempComponent {...props} />)).toMatchSnapshot()
    })
})