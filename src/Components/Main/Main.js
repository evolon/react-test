import React from 'react'
import { compose, lifecycle } from 'recompose'
import { withWeatherInfo } from '../../Containers/weather/withWeatherInfo'
import { ForecastDays } from '../ForecastDays/ForecastDays'
import { ForecastCity } from '../ForecastCity/ForecastCity'
import { DEFAULT_CITY_NAME, DEFAULT_COUNTRY_CODE } from './constants'
import './style.css'

export const MainComponent = ({weather, image, isLoading, loadCityForecast, error}) => 
    <main>
        <ForecastCity 
            error={error}
            isLoading={isLoading} 
            weather={weather} 
            image={image} 
            loadCityForecast={loadCityForecast} />
        <ForecastDays 
            weather={weather} />
    </main>

const enhance = compose(
    withWeatherInfo,
    lifecycle({
        async componentDidMount() {
            await this.props.loadCityForecast({
                countryCode: DEFAULT_COUNTRY_CODE,
                cityName: DEFAULT_CITY_NAME
            })
        }
    })
)

export const Main = enhance(MainComponent)
export default Main