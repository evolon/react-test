import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import { MainComponent } from './Main'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('<MainComponent />', () => {
    it('should match the snapshot', async () => {
        const props = {
            weather: {},
            image: {},
            isLoading: false,
            loadCityForecast: jest.fn()
        }

        expect(shallow(<MainComponent {...props} />)).toMatchSnapshot()
    })
})
