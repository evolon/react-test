import React from 'react'
import { TextBox } from '../Fields/TextBox'
import { DropDown } from '../Fields/DropDown'
import { withState, withProps, compose } from 'recompose';
import { Spinner } from '../Spinner/Spinner'
import './style.css'

export const SearchComponent = ({isLoading, locationInfo, changeLocationInfo, loadCityForecast, countries, error}) => 
    <form className="search-field" onSubmit={e => e.preventDefault() || loadCityForecast(locationInfo)}>
        <DropDown
            value={locationInfo.countryCode}
            options={countries}
            onChange={e => changeLocationInfo({
                cityName: '',
                countryCode: e.value
            })} />
        <TextBox
            isDisabled={isLoading}
            value={locationInfo.cityName}
            placeholder="City Name..."
            onChange={e => changeLocationInfo({
                ...locationInfo,
                cityName: e.target.value
            })} 
            onIconClick={e => e.preventDefault() || loadCityForecast(locationInfo)}
            icon={
                isLoading ?
                    <Spinner className="loading-spinner" /> :
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path className="shape" d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
                        <path d="M0 0h24v24H0z" fill="none"/>
                    </svg>
            }/>
            <span class="error-field">
                {error}
            </span>
    </form>

const enhance = compose(
    withState('locationInfo', 'changeLocationInfo', {
        countryCode: 'ch',
        cityName: 'Zurich'
    }),
    withProps({
        countries: [
            {
                value: 'ch',
                text: 'Switzerland'
            },
            {
                value: 'fr',
                text: 'France'
            },
            {
                value: 'cn',
                text: 'China'
            },
            {
                value: 'li',
                text: 'Lithuania'
            }
        ],
    }),
)

export const Search = enhance(SearchComponent)