import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import { SearchComponent } from './Search'
import Adapter from 'enzyme-adapter-react-16'
import { DropDown } from '../Fields/DropDown'
import { TextBox } from '../Fields/TextBox'

Enzyme.configure({ adapter: new Adapter() })

describe('<SearchComponent />', () => {
    let props
    let wrapper

    beforeEach(() => {
        props = {
            isLoading: false,
            locationInfo: {
                countryCode: 'tc',
                cityName: 'Test City'
            },
            changeLocationInfo: jest.fn(),
            loadCityForecast: jest.fn(),
            countries: [
                'Test country #1',
                'Test country #2',
                'Test country #3'
            ]
        }

        wrapper = shallow(<SearchComponent {...props} />)
    })

    it('should match the snapshot in the default state', async () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should call the changeLocationInfo when changing either fields', async () => {
        wrapper.find(DropDown).prop('onChange')({
            countryCode: 'tcfdd',
            cityName: 'Test City From DropDown'
        })

        wrapper.find(TextBox).prop('onChange')({
                target: {
                countryCode: 'tcftb',
                cityName: 'Test City from TextBox'
            }
        })

        expect(props.changeLocationInfo.call.length).toBe(1)
    })

    it('should call the loadCityForecast on submit', async () => {
        wrapper.find('form').prop('onSubmit')({
            preventDefault: () => {}
        })

        expect(props.loadCityForecast.call.length).toBe(1)
    })
})