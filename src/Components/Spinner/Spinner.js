import React from 'react'
import spinnerImage from './spinner.gif'

const SpinnerComponent = () =>
    <img className="loading-spinner" src={spinnerImage} alt="" />

export const Spinner = SpinnerComponent