import React from 'react'
import icons from './weatherIcons'

const WeatherIconComponent = ({ weather }) => 
    <div className="icon">
        {icons[weather.icon]}
    </div>

export const WeatherIcon = WeatherIconComponent