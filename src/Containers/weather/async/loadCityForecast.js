import { LOAD_CITY_FORECAST, SET_ERROR, CLEAR_ERROR } from '../constants'
import { fetchCityForecast } from '../../../api/weather-api'

export const loadCityForecast = (payload) => async (dispatch) => {
    try {
        const cityForecastData = await fetchCityForecast(payload)

        dispatch({
            type: LOAD_CITY_FORECAST,
            payload: cityForecastData
        })

        dispatch({
            type: CLEAR_ERROR
        })
    } catch ({ response }) {
        dispatch({
            type: SET_ERROR,
            payload: response.data.message
        })
    }
}