import { LOAD_CITY_IMAGE } from '../constants'
import { fetchCityImage } from '../../../api/images-api'

export const loadCityImage = (payload) => async (dispatch) => {
    try{
        const cityImageData = await fetchCityImage(payload)
        
        dispatch({
            type: LOAD_CITY_IMAGE,
            payload: cityImageData
        })
    } catch (error) {
        console.warn(error)
    }
}