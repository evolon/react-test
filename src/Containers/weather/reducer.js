import { LOAD_CITY_FORECAST, LOAD_CITY_IMAGE, SET_ERROR, CLEAR_ERROR } from './constants'

const INITIAL_STATE = {}

export const weatherReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case LOAD_CITY_FORECAST: 
            return {
                ...state,
                weather: payload
            }
        case LOAD_CITY_IMAGE:
            return {
                ...state,
                image: payload
            }
        case SET_ERROR:
            return {
                ...state,
                error: payload
            }
        case CLEAR_ERROR:
            return {
                ...state,
                error: null
            }
        default:
            return state
    }
}