import { weatherReducer } from './reducer'
import { LOAD_CITY_FORECAST, LOAD_CITY_IMAGE, ADD_ERROR } from './constants'

describe('weather info reducer', () => {
    it('should return the initial state', () => {
        expect(weatherReducer(undefined, {})).toEqual({})
    })

    it('should add the loaded city forecast info', () => {
        expect(weatherReducer(undefined, {
            type: LOAD_CITY_FORECAST,
            payload: {
                test: 'test'
            }
        })).toEqual({
            weather: {
                test: 'test'
            }
        })

        expect(weatherReducer({
            weather: {
                test: 'old value'
            }
        }, {
            type: LOAD_CITY_FORECAST,
            payload: {
                test: 'new value'
            }
        })).toEqual({
            weather: {
                test: 'new value'
            }
        })
    })

    it('should add the loaded city image', () => {
        expect(weatherReducer(undefined, {
            type: LOAD_CITY_IMAGE,
            payload: {
                test: 'test'
            }
        })).toEqual({
            image: {
                test: 'test'
            }
        })

        expect(weatherReducer({
            image: {
                test: 'old value'
            }
        }, {
            type: LOAD_CITY_IMAGE,
            payload: {
                test: 'new value'
            }
        })).toEqual({
            image: {
                test: 'new value'
            }
        })
    })

    it('should add the error', () => {
        expect(weatherReducer(undefined, {
            type: ADD_ERROR,
            payload: 'test error text'
        })).toEqual({
            errors: [
                'test error text'
            ]
        })

        expect(weatherReducer({
            errors: [
                'old error text'
            ]
        }, {
            type: ADD_ERROR,
            payload: 'new error text'
        })).toEqual({
            errors: [
                'old error text',
                'new error text'
            ]
        })
    })
})