import { ADD_ERROR } from '../constants'

export const addError = payload => ({
    type: ADD_ERROR,
    payload
})
