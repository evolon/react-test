import { addError } from './addError'
import { ADD_ERROR } from '../constants'

describe('actions', () => {
    it('Should create an action to add errors', () => {
        const error = 'Test error'
        const expectedAction = {
            type: ADD_ERROR,
            payload: error
        }

        expect(addError(error)).toEqual(expectedAction)
    })
})