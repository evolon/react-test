import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loadCityForecast } from './async/loadCityForecast'
import { loadCityImage } from './async/loadCityImage'
import _ from 'lodash'
import moment from 'moment'

// exporting non connected component for unit testing
export const withWeatherHoC = InnerComponent => {
    return class WeatherHoC extends Component {
        constructor(props) {
            super(props)
            this.loadCityForecast = this.loadCityForecast.bind(this)
        }

        state = {
            isLoading: false,
            dailyForecast: {}
        }

        async loadCityForecast({ cityName, countryCode }) {
            this.setState({
                isLoading: true
            })

            try {
                await Promise.all([
                    this.props.loadCityForecast({
                        cityName,
                        countryCode
                    }),
                    this.props.loadCityImage({
                        cityName,
                    })
                ])
            } finally {   
                this.setState({
                    isLoading: false
                })
            }
        }

        render () {
            return (
                <InnerComponent 
                    error={this.props.error}
                    weather={this.props.weather} 
                    image={this.props.image}
                    loadCityForecast={this.loadCityForecast}
                    isLoading={this.state.isLoading} />
            )
        }
    }
}

export const withWeatherInfo = InnerComponent => {
    const WeatherHoC = withWeatherHoC(InnerComponent)

    const mapStateToProps = ({ weatherInfo }) => {
        let props = {...weatherInfo}

        // merging daily measurements into a single measure
        if (weatherInfo.weather && weatherInfo.weather.list) {
            const days = _(weatherInfo.weather.list)
            .groupBy(measure => moment(measure.dt, 'X').dayOfYear())
            .map(day => day[0])
            .value()
            
            props = {
                error: weatherInfo.error,
                image: weatherInfo.image,
                weather: {
                    ...weatherInfo.weather,
                    list: days
                },
            }
        }

        return props;
    }

    const mapDispatchToProps = {
        loadCityForecast,
        loadCityImage
    }

    return connect(
        mapStateToProps,
        mapDispatchToProps
    )(WeatherHoC)
}