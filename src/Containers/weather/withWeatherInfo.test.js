import React from 'react'
import { withWeatherHoC } from './withWeatherInfo'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('<WithWeatherInfo />', () => {
    let wrapper;
    let mockActions;

    beforeEach(() => {
        const MockChild = ({ loadCityForecast }) => 
            <div>
                <button id="loadCityForecast" onClick={() => loadCityForecast({
                    cityName: 'Test City',
                    countryCode: 'tc'
                })} />
            </div>

        const EnhancedWeatherInfo = withWeatherHoC(MockChild)

        mockActions = {
            loadCityForecast: jest.fn(),
            loadCityImage: jest.fn(),
        }

        wrapper = mount(<EnhancedWeatherInfo {...mockActions} />)
    })

    it(
        `should be able to load city forecast on child component request 
        and change the isLoading state while loading`, async () => {
        expect(wrapper.find('MockChild').at(0).props().isLoading).toBe(false)

        wrapper.find('#loadCityForecast').simulate('click', {
            preventDefault() {}
        })

        expect(wrapper.find('MockChild').at(0).props().isLoading).toBe(true)

        expect(mockActions.loadCityForecast.mock.calls.length).toBe(1)
        expect(mockActions.loadCityImage.mock.calls.length).toBe(1)
    })
}) 