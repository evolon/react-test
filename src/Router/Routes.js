import React from 'react'
import { Route, Switch } from 'react-router-dom'

import { Main } from '../Components/Main/Main'
import { HOME_PATH } from '../constants'

export const Routes = () => {
    return (
        <Switch>
            <Route path={HOME_PATH} component={Main} exact />
        </Switch>
    )
}