import axios from 'axios'
import { UNSPLASH_ACCESS_KEY } from './constants'

export const fetchCityImage = async ({cityName, weatherCondition}) => {
    const result = await axios.get(`https://api.unsplash.com/photos/random?query=${cityName}`, {
        headers: {
            'Authorization': `Client-ID ${UNSPLASH_ACCESS_KEY}`
        }
    })
    return result
}