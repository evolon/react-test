import axios from 'axios'
import { OPEN_WEATHER_API_KEY } from './constants'

export const fetchCityForecast = async ({cityName, countryCode}) => {
    const { data } = await axios.get(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName},${countryCode}&appid=${OPEN_WEATHER_API_KEY}`)
    return data
}