import { combineReducers } from "redux"
import { weatherReducer } from '../Containers/weather/reducer'

export const rootReducer = combineReducers({
    weatherInfo: weatherReducer
})